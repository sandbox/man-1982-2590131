<?php

/**
 * Create/update poducts catalog
 * $vars['voc_catalog'] - machine name of vocabulary catalog
 * $vars['name'] - element name
 * $vars['uuid'] - uuid term
 * $vars['parent_uuid'] - uuid of parent elements if empty than root element
 */
function sm_ex21c_crud_catalog($vars) {
  $uuid = $vars['uuid'];
  $parent_uuid = $vars['parent_uuid'];
  $name = $vars['name'];
  $uuid_entities = entity_uuid_load('taxonomy_term', array($uuid));
  if(!empty($parent_uuid)){
    $uuid_entities_parents = entity_uuid_load('taxonomy_term', array($parent_uuid));
    if(empty($uuid_entities_parents)) {
//      dpm($uuid_entities_parents, 'Empty parent uuid' . $parent_uuid . ':' . __FUNCTION__);
    }
  }
  $voc = taxonomy_vocabulary_machine_name_load(variable_get_value('sm_ex21c_settings:catalog'));
  if(empty($uuid_entities)) {
    $term = entity_create('taxonomy_term',
      array(
        'name'    => $name,
        'vid'     => $voc->vid,
        'format'  => 'full_html',
        'parent'  => empty($uuid_entities_parents) ? array(0) : array_keys($uuid_entities_parents),
//        'uuid'    => $uuid
      ));
    $term->uuid = $uuid;

    try{
      // Use only entity save becouse we saved UUID in database
      entity_save('taxonomy_term', $term);
//      dpm($uuid, 'current : '. $term->uuid);
    }
    catch(Exception $e) {

      watchdog('1C:CATALOG', $e->getMessage() .':' . $term->name . ':' .var_export($term, TRUE));
    }
  }
}

/**
 *  $vars
 *  $vars[name]         = $product->Наименование . '';
 *  $vars[sku]          = $product->Артикул . '';
 *  $vars[catalog_uuid] = $product->Группы->Ид . '';
 *  $vars[img_path]     = $product->Картинка . '';
 *  $vars[unit]         = $product->БазоваяЕдиница. '' единицы измерения;
 */
function sm_ex21c_crud_product($vars) {
  $uuid             = $vars['uuid'];
  $uuid_entities    = entity_uuid_load('node', array($uuid));
  $title          = $vars['name'];
  $sku            = $vars['sku'];
  $catalog_uuid   = $vars['catalog_uuid'];
  $img_path       = $vars['img_path'];
  $catalog_uuid_entities = entity_uuid_load('taxonomy_term', array($catalog_uuid));
  if(empty($catalog_uuid_entities)) {
//    dpm( $catalog_uuid_entities,' Empty catalog in create product : ' . $catalog_uuid );
  }
  $field_info       = field_info_field('uc_product_image');
  $field_instance   = field_info_instance('node', 'uc_product_image', 'product');

  $dest_dir = file_field_widget_uri($field_info, $field_instance);

  if(file_prepare_directory($dest_dir, FILE_CREATE_DIRECTORY) && $image = sm_tools_file_uri_to_object(variable_get_value('sm_ex21c_settings:folder_import') .'/' .  $img_path, TRUE)) {
    //$new_img = file_move($image, $dest_dir, FILE_EXISTS_REPLACE);
    $new_img = file_copy($image, $dest_dir, FILE_EXISTS_REPLACE);
  };


  if(empty($uuid_entities)) {
    $product = entity_create('node', array(
      'type'                => 'product_display',
      'uid'                 => 1,
      'status'              => 1,
      'title'               => $title,
      'field_catalog'       => array_keys($catalog_uuid_entities),
      'uuid'                => $uuid
      ));
    try{
      entity_save('node', $product);
    }
    catch (Exception $e){
      sm_ex21c_debug_function($e);
      watchdog('1C:PRODUCT', $e->getMessage());
      return false;
    }
    $wr_product = entity_metadata_wrapper('node', $product);
//    dpm($catalog_uuid_entities, '$catalog_uuid_entities');
    foreach($catalog_uuid_entities as $term_catalog) {
      $wr_product->field_catalog[] = $term_catalog->tid;
    }

    if(!empty($new_img->fid)) {
      $wr_product->uc_product_image[] = array('fid' => $new_img->fid);
    }

    try{
      // Use only entity save becouse we saved UUID in database
      $wr_product->save();
      if(variable_get_value('sm_ex21c_settings:develop')) {
        if($uuid != $product->uuid) {
          watchdog('1C:UUID', 'Input UUID:' . $uuid . ' Saved UUID:' . $product->uuid);
        }
      }
    }
    catch(Exception $e) {
      watchdog('1C:PRODUCT', $e->getMessage() . ':' . $product->title . ':' . var_export($product, TRUE));
    }
  }
}

/**
 * Update stock and price
 * $vars['uuid']
 * $vars['price'] array type_price_uuid => value
 * $vars['count']
 */
function sm_ex21c_crud_price_and_stock($vars) {
  $uuid   = $vars['uuid'];
  $prices_array  = empty($vars['prices'])?array(0):$vars['prices'];
  $count  = $vars['count'];

  $uuid_entities = entity_uuid_load('node', array($uuid));
  foreach($uuid_entities as $entity) {
    $product = node_load($entity->nid);
      if(count($prices_array) == 1) {
        $price = reset($prices_array);
      }
    else {
      //TODO Added settings vars for Price
      $price = reset($prices_array);
    }
    $product->sell_price = empty($price) ? 0: $price;
    $product->price = empty($price) ? 0: $price;
    try{
      node_save($product);

      db_merge('uc_product_stock')
        ->key(array('sku' => $product->model))
        ->updateFields(array(
          'active' => 1,
          'stock' => $count,
          'threshold' => 0,
        ))
        ->insertFields(array(
          'sku' => $product->model,
          'active' => 1,
          'stock' => $count,
          'threshold' => 0,
          'nid' => $product->nid,
        ))
        ->execute();

    }
    catch(Exception $e) {
      watchdog('1C:PRODUCT', $e->getMessage() .':' . $product->title . ':' . var_export($product, TRUE));
    }
  }
}
