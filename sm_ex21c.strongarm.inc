<?php
/**
 * @file
 * sm_ex21c.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function sm_ex21c_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_uuid';
  $strongarm->value = array(
    'comment'                     => TRUE,
    'commerce_customer_profile'   => 0,
    'commerce_line_item'          => 0,
    'commerce_order'              => 0,
    'commerce_product'            => 0,
    'characteristics_product'     => TRUE,
    'node'                        => TRUE,
    'file'                        => TRUE,
    'taxonomy_term'               => TRUE,
    'taxonomy_vocabulary'         => TRUE,
    'user'                        => TRUE,
    'rules_config'                => 0,
  );
  $export['entity_uuid'] = $strongarm;

  return $export;
}
