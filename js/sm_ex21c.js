(function($) {

    /**
     * Handler for form settings
     */
    Drupal.behaviors.SMEX21cAdminFormSettings = {
        attach: function (context, settings) {
           var  $context = $(context);
           $('[name="sm_ex21c_settings:entity_type"]', $context).once(function (){
               var $title_label = '' ; // label of checked radio
               var $select = $('[name="sm_ex21c_settings:field_catalog"]', $context);

               var $this_entity_type = $(this);
               if($this_entity_type.prop('checked')){
                   $title_label = $.trim($this_entity_type.closest('div').find("label").text());
                   $('optgroup', $select).each(function(){
                       $(this).hide();
                       var label = $.trim($(this).attr("label"));
                       if(label == $title_label){
                           $(this).show();
                       }
                   });

               }
               /**
                * Handler onchange to nodes product select
                */
               $(this).change(function () {
                   // Set default value on change
                   $($select).val('none');
                   var $this = $(this);
                   if($this.prop('checked')){
                       $title_label = $.trim($this.closest('div').find("label").text());
                       if(window.console){
                           console.log($title_label);
                       }
                      $('optgroup', $select).each(function(){
                          $(this).hide();
                          var label = $.trim($(this).attr("label"));
                          if(label == $title_label){
                              $(this).show();
                          }
                      });

                   }
               });

           });
        }
    };

})(jQuery);
