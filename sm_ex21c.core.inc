<?php
/**
 * @file
 * main callback s for modules
 */


function sm_ex21c_process() {
  $query = drupal_get_query_parameters($_GET, array('q', 'page'));

  if(variable_get_value('sm_ex21c_settings:develop')) {
    $headers = sm_ex21c_get_headers();
    watchdog('1C:INPUT', var_export($_GET + $headers, TRUE));
  }

  if(($query['mode'] != 'checkauth')&& !sm_ex21c_process_checktoken($query)) {

  }

  switch($query['mode']) {
    case 'checkauth':
      return sm_ex21c_process_checkauth($query);
      break;
    case 'init':
      return sm_ex21c_process_init($query);
      break;
    case 'file':
      return sm_ex21c_process_file($query);
      break;

    case 'import':
      return sm_ex21c_process_import($query);
      break;
  }
}

function sm_ex21c_process_checktoken($query) {

  return TRUE;
}


/**
 * callback in mode checkauth
 */
function sm_ex21c_process_checkauth($query) {
  print "success" . "\n";
  print "1C-Exchange" . "\n";
  print variable_get_value('sm_ex21c_settings:autorisation_code') ;

  drupal_page_footer();
}

/**
 * callback in mode init
 */
function sm_ex21c_process_init($query) {
  $responce[] = 'zip=no';
  $responce[] = 'file_limit=' .  file_upload_max_size();
  print implode("\n", $responce);
  drupal_page_footer();
}
/**
 * callback in mode file
 */
function sm_ex21c_process_file($query) {
  //Retrieve data from stream
  $data = file_get_contents('php://input');
  $dump = var_export($data, TRUE);
  watchdog('1C:FILE', $query['filename'] . ' : ' );

  $name = drupal_basename($query['filename']);
  $dest_dir = variable_get_value('sm_ex21c_settings:folder_import') . '/' . str_replace($name, '', $query['filename']);

  if(!file_prepare_directory($dest_dir, FILE_CREATE_DIRECTORY)) {
    $responce[] = 'Derictory "' . $dest_dir  . '" not create. Check permissions';
  };

  if (file_unmanaged_save_data($data, $dest_dir . $name, FILE_EXISTS_REPLACE)) {
    $responce[] = 'success';
  }
  else {
    $responce[] = 'progress';
  };


  ;
  print implode("\n", $responce);
  drupal_page_footer();
}

/*
 * Helper function for retrieve header requests information
 */
function sm_ex21c_get_headers($header_name = null){
  $keys = array_keys($_SERVER);
  $headervals = array();
  if(is_null($header_name)) {
    $headers = preg_grep("/^HTTP_(.*)/si", $keys);
  }
  else {
    $header_name_safe = str_replace("-", "_", strtoupper(preg_quote($header_name)));
    $headers = preg_grep("/^HTTP_${header_name_safe}$/si", $keys);
  }

  foreach($headers as $header) {
    if(is_null($header_name)){
      $headervals[substr($header, 5)]=$_SERVER[$header];
    }
    else {
      return $_SERVER[$header];
    }
  }

  return $headervals;
}


/**
 * @param $query
 */
function sm_ex21c_process_import($query) {
  $responce[] = 'success';
  print implode("\n", $responce);
  drupal_page_footer();
}