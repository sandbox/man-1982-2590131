<?php
/**
 * Created by PhpStorm.
 * User: kochergin
 * Date: 10.03.15
 * Time: 17:22
 */

function sm_ex21c_exchange_form($form, &$form_state) {
  $form = array();
  $folder_name = variable_get_value('sm_ex21c_settings:folder_import');
//  $scan_result = file_scan_directory($folder_name , '/xml|zip/'); //'/(\.\.?|CVS)$/'
  $scan_result = file_scan_directory($folder_name , '/\.(?:z(?:ip|[0-9]{2})|r(?:ar|[0-9]{2})|jar|bz2|gz|tar|rpm|xml)$/i'); //'/(\.\.?|CVS)$/'

  $list_file = t('Directory is empty');
//  sm_ex21c_debug_function($scan_result);
  $file_name = array();
  foreach ($scan_result as $key => $value){
    $file_name[] = l($value->filename . ' <b>' . format_size(filesize($value->uri)) . '</b>', file_create_url($value->uri), array('html' => TRUE));
  }

  if(!empty($file_name)){
    $list_file = implode('<br>', $file_name);
  }

//  dpm($scan_result);

  $form['import'] = array(
    '#type'         => 'fieldset',
    '#title'        => 'Import',
    '#description'  => t('Import from files in public directories'),
    '#colabsible'   => TRUE,
  );

  $form['import']['scan_directory'] = array(
    '#markup'       => '<div class="folder-echange-list">' . t('Folder for import files : <b>@folder_name </b></br>', array('@folder_name' => $folder_name)) .  $list_file . '</div>',
  );

  $form['import']['submit_catalog_and_products'] = array(
    '#type'       => 'submit',
    '#value'      => t('Import catalog and products'),
    '#submit'     => array('sm_ex21c_import_catalog_and_products_form_submit'),
  );
  $form['import']['update_stock_and_price'] = array(
    '#type'       => 'submit',
    '#value'      => t('Update stock and Price'),
    '#submit'     => array('sm_ex21c_update_stock_and_price_form_submit'),
  );
  return $form;
}

/**
 * Callback for submit form
 */
function sm_ex21c_import_catalog_and_products_form_submit(&$form, &$form_state) {
  $voc = taxonomy_vocabulary_machine_name_load(variable_get_value('sm_ex21c_settings:catalog'));
if(empty($voc->vid)) {
  drupal_set_message('Cannot set <b>catalog</b> variable. Got to settings and set it. ' . l('Settings 1c exchange', 'admin/config/development/1c-settings/vars'));
  drupal_goto('admin/config/development/1c-settings');
}
  $values = $form_state['values'];
  $batch  = array();
  $batch['operations'] = array();
  $folder_name = variable_get_value('sm_ex21c_settings:folder_import');
  $scan_result = file_scan_directory($folder_name , '/(import*)(?:z(?:ip|[0-9]{2})|r(?:ar|[0-9]{2})|jar|bz2|gz|tar|rpm|xml)$/i'); //'/(\.\.?|CVS)$/'

    $list_file = t('Directory is empty');
//  sm_ex21c_debug_function($scan_result);
    $file_name = array();
    // Scan directory, extract xml and process its
    dpm($scan_result);
    foreach ($scan_result as $key => $value){
        $uri_import = $value->uri;
        if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri_import)) {
            $path = $wrapper->realpath();
            $xml = simplexml_load_file($path);
//    sm_ex21c_debug_function($xml->asXML());
            $groups = $xml->Классификатор->Группы->Группа;
            $products = $xml->Каталог->Товары->Товар;
            foreach($groups as $group) {
//      dpm($group);
                $vars['group'] = $group;
                sm_ex21c_debug_function($group->Наименование->__toString());
                sm_ex21c_extract_groups($vars, $batch);
            }

            $vars = array();
            foreach($products as $product) {
                $vars['product'] = $product;
                sm_ex21c_extract_products($vars, $batch);
            }
        }
    }




  $batch['finished'] = 'sm_ex21c_exchange_batch_finished';
  $batch['file']     = drupal_get_path('module', 'sm_ex21c') . '/sm_ex21c.admin.inc';
  batch_set($batch);
}


/**
 * Create batch operations from groups of 1C Products
 * if groups contain sub group batch run recursive
 * @param $vars
 * @param $batch
 */
function sm_ex21c_extract_groups($vars, &$batch) {
  $group = $vars['group'];
  $uuid = $group->Ид->__toString();
  $name = $group->Наименование->__toString();
  $parent_uuid = empty($vars['parent_uuid'])? '': $vars['parent_uuid'] . '';

  $input = compact('uuid', 'parent_uuid', 'name');
  $batch['operations'][] = array('sm_ex21c_crud_catalog_batch', array($input));
  $new_batch = array();
  if(isset($group->Группы->Группа)) {
    $sub_groups = $group->Группы->Группа;
    foreach($sub_groups as $sub_group) {
      $vars['group']        = $sub_group;
      $vars['parent_uuid']  = $uuid;
      sm_ex21c_extract_groups($vars, $new_batch);
      $batch['operations'] = array_merge($batch['operations'], $new_batch['operations']);
    }
  }

}

/**
 *callback batch for crud catalog
 */
function sm_ex21c_crud_catalog_batch($input, &$context) {
//  dpm($input);
  module_load_include('inc', 'sm_ex21c', 'includes/sm_ex21c');
  $context['message'] = 'Create/Update catalog term : ' . $input['name'];
  sm_ex21c_crud_catalog($input);
}

/**
 * Make bathc operations array
 */
function sm_ex21c_extract_products($vars, &$batch) {
  $product = $vars['product'];
  $uuid         = $product->Ид->__toString();
  $name         = $product->Наименование->__toString();
  $sku          = $product->Артикул->__toString();
  $catalog_uuid = $product->Группы->Ид->__toString();
  $img_path     = $product->Картинка->__toString();
  $unit         = $product->БазоваяЕдиница->__toString();
  //dpm(compact('name', 'sku', 'catalog_uuid', 'img_path', 'unit', 'uuid  ', 'product'));
  $input = compact('name', 'sku', 'catalog_uuid', 'img_path', 'unit', 'uuid');
  $batch['operations'][] = array('sm_ex21c_crud_product_batch', array($input));
}

/**
 * Batch operations for create products
 */
function sm_ex21c_crud_product_batch($input, &$context) {
//  dpm($input);
  module_load_include('inc', 'sm_ex21c', 'includes/sm_ex21c');
  $context['message'] = 'Create/Update product :' . $input['name'] . ' SKU:' . $input['sku'];
  sm_ex21c_crud_product($input);
}

/**
 *
 */
function sm_ex21c_exchange_batch_finished($success, $results, $operations) {
  $message = implode('<br>', $results);
  drupal_set_message($message, 'Finished operations');
}

/**
 * Submit callback
 * @param $form
 * @param $form_state
 */
function sm_ex21c_update_stock_and_price_form_submit(&$form, &$form_state) {

  $voc = taxonomy_vocabulary_machine_name_load(variable_get_value('sm_ex21c_settings:catalog'));

  if(empty($voc->vid)) {
    drupal_set_message('Cannot set <b>catalog</b> variable. Got to settings and set it. ' . l('Settings 1c exchange', 'admin/config/development/1c-settings/vars'));
    drupal_goto('admin/config/development/1c-settings');
  }

  $values = $form_state['values'];
  $batch  = array();
  $batch['operations'] = array();
  $folder_name = variable_get_value('sm_ex21c_settings:folder_import');
  $uri_import = $folder_name . '/offers.xml';

  if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri_import)) {
    $path = $wrapper->realpath();
    $xml = simplexml_load_file($path);
    sm_ex21c_debug_function($xml);
    $offers_packets = $xml->ПакетПредложений->Предложения->Предложение;
    foreach($offers_packets as $packet) {
      $prices = array();
      $count = $packet->Количество . '';
      $count = empty($count) ? 0 : $count;
      $uuid = $packet->Ид . '';
      sm_ex21c_debug_function($packet, 'Offers item. склад:' . $count );
      if(isset($packet->Цены->Цена)) {
        foreach($packet->Цены->Цена as $price) {
//          dpm($price->ЦенаЗаЕдиницу . '', 'ЦенаЗаЕдиницу');
//          dpm($price->Представление . '', 'Представление');
          $prices[$price->ИдТипаЦены . ''] = $price->ЦенаЗаЕдиницу . '';
//          dpm($price->ЦенаЗаЕдиницу . 'ЦенаЗаЕдиницу');
        }
      }

      $input = compact('uuid', 'count', 'prices');
      $batch['operations'][] = array('sm_ex21c_price_stock_crud_batch', array($input));
    }
  }
  else {
    drupal_set_message('Not <b>offers.xml</b> in <b>' . $folder_name .'</b>. Check file and continue');
  }
  $batch['finished'] = 'sm_ex21c_exchange_batch_finished';
  $batch['file']     = drupal_get_path('module', 'sm_ex21c') . '/sm_ex21c.admin.inc';

  batch_set($batch);
}

/**
 * @param $vars
 * @param $context
 */
function sm_ex21c_price_stock_crud_batch($vars, &$context) {
  module_load_include('inc', 'sm_ex21c', 'includes/sm_ex21c');
  $uuid_entities = entity_uuid_load('node', array($vars['uuid']));
  foreach($uuid_entities as $product) {
    $context['results'][] = l($product->title, 'node/' . $product->nid) . ' set stock <b>' . $vars['count'] . '</b>
    and price : ' . var_export($vars['prices'], TRUE);
    $context['message'] = 'Create/Update stock and price in  ' . $product->title . ' Stock:' . $vars['count'];
  }
  sm_ex21c_crud_price_and_stock($vars);
}
