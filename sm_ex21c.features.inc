<?php
/**
 * @file
 * sm_ex21c.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sm_ex21c_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
